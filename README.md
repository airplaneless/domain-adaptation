# Unsupervised domain adaptation
## MNIST to SVHN

### by backpropagation

#### Usual LeNet model:

![logo](misc/lenet_example.png)
![logo](misc/lenet_confmat.png)

             precision    recall  f1-score   support

        0.0       0.13      0.14      0.13      4948
        1.0       0.83      0.17      0.28     13861
        2.0       0.50      0.24      0.32     10585
        3.0       0.77      0.10      0.17      8497
        4.0       0.13      0.73      0.22      7458
        5.0       0.75      0.15      0.25      6882
        6.0       0.34      0.20      0.25      5727
        7.0       0.37      0.28      0.32      5595
        8.0       0.15      0.18      0.16      5045
        9.0       0.36      0.04      0.07      4659

    avg / total       0.50      0.23      0.23     73257


#### LeNet with DA:

![logo](misc/dalenet_example.png)
![logo](misc/dalenet_confmat.png)

             precision    recall  f1-score   support

        0.0       0.47      0.11      0.18      4948
        1.0       0.58      0.38      0.46     13861
        2.0       0.36      0.57      0.44     10585
        3.0       0.26      0.25      0.25      8497
        4.0       0.22      0.14      0.18      7458
        5.0       0.31      0.58      0.40      6882
        6.0       0.13      0.15      0.14      5727
        7.0       0.34      0.54      0.42      5595
        8.0       0.28      0.18      0.22      5045
        9.0       0.45      0.17      0.25      4659

    avg / total       0.36      0.34      0.32     73257



